self: super: 
let 
  doJailbreak = super.haskell.lib.doJailbreak;
  dontCheck = super.haskell.lib.dontCheck;
  dontHaddock = super.haskell.lib.dontHaddock;
in 
{
  haskell = super.haskell // {
    packages = super.haskell.packages // {
      ghc863 = super.haskell.packages.ghc863.override {
        overrides = newSelf: newSuper: {
          # no overrides yet
        };
      };
    };
  };
}
