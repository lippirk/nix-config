*Overlays* extend `nixpkgs`.
According to the docs, you can put a list of overlays in `~/.config/nixpkgs/overlays.nix` or list them in the directory `~/.config/nixpkgs/overlays/`. I am doing the latter here.

For example:

```
 # assume my-nvim declared in an overlay
 [ben@tigger:~]$ nix-shell -p my-nvim
 [nix-shell:~]$ nvim  
 # executes neovim as declared in my neovim overlay
```

Similarly, observe:

```
nix-repl> pkgs = import <nixpkgs> {}

nix-repl> pkgs.my-nvim
«derivation /nix/store/19sba1zi4b0vx189ycmcznz4bp8jjzk1-neovim-0.3.1.drv»
```

That is, importing nixpkgs implicitly imports your local overlays too.
