self: super:
let
  base-vimrc = builtins.readFile ./nvim/base-vimrc;
  haskell-vimrc = builtins.readFile ./nvim/haskell-vimrc;

  basePlugins = with super.pkgs.vimPlugins; [
    nerdtree
    lightline-vim
    vim-fugitive
    neoformat
    vim-commentary
    vim-indent-guides
    ack-vim
    vim-colorschemes
    vim-polyglot
  ];
  haskellPlugins = with super.pkgs.vimPlugins; [ ale ];

  my-nvim = super.neovim.override {
    vimAlias = false;
    configure = {
      customRC = base-vimrc;
      packages.myVimPackage = {
        start = basePlugins;
      };
    };
  };

  # programs used in base-vimrc
  my-nvim-dependencies = [
    super.pkgs.nodePackages.prettier
  ];

  my-haskell-nvim = { moduleSearchPaths ? "src", ignoreLinters ? "[]" } : my-nvim.override {

    configure = {
      customRC = ''
        ${base-vimrc}
        "ALE

        "lint
        let g:ale_echo_msg_format = '%linter% says %s'
        let g:ale_linters_ignore = ${ignoreLinters}

        let g:ale_linters = {'haskell' : ['hlint', 'ghc']}
        let g:ale_haskell_ghc_options = '-fno-code -v0 -i${moduleSearchPaths}'

        "completion doesn't seem to work
        "let g:ale_completion_enabled=1

        "let g:ale_list_window_size = 5
        let g:ale_warn_about_trailing_whitespace=1

        "neoformat
        let g:neoformat_enabled_haskell = ['hfmt']
      '';

      packages.myVimPackage = {
        start = basePlugins ++ haskellPlugins;
      };
    };
  };

in
  {
    my-nvim              = my-nvim;
    my-nvim-dependencies = my-nvim-dependencies;
    my-haskell-nvim      = my-haskell-nvim;
  }
