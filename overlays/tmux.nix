self: pkgs:
let
   my-tmux = pkgs.symlinkJoin {
    name = "tmux";
    buildInputs = [ pkgs.makeWrapper ];
    paths = [ pkgs.tmux ];
    postBuild = ''
      wrapProgram "$out/bin/tmux" \
      --add-flags "-f ${./tmux/tmux.conf}"
      '';
    };
in
{ my-tmux = my-tmux; }
