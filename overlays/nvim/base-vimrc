let mapleader = "\<Space>"
set encoding=utf-8

"reload buffers when they are changed on file system
set autoread

"allow backspacing over everything in insert mode
set backspace=indent,eol,start

"store lots of :cmdline history
set history=1000

set showcmd     "show incomplete cmds down the bottom
set showmode    "show current mode down the bottom

set number      "show line numbers

"display tabs and trailing spaces
set list
set listchars=tab:▷⋅,trail:⋅,nbsp:⋅

set hlsearch    "hilight searches by default

set wrap        "wrap lines
set linebreak   "wrap lines at convenient points

if v:version >= 703
  "undo settings
  set undodir=~/.vim/undofiles
  set undofile

  set colorcolumn=+1 "mark the ideal max text width
endif

"default indent settings
set shiftwidth=2
set softtabstop=2
set tabstop=2

"move blocks of code around
" left <> right
vnoremap <C-l> >gv
vnoremap <C-h> <gv
" up <> down
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

"resize buffer
nnoremap + <C-W>+
nnoremap - <C-W>-

set expandtab
set autoindent

"fixes unicode issue
set guicursor=

"colorscheme
colorscheme onedark

"nerdtree
let g:nerdtree_tabs_autoclose=0
let NERDTreeShowHidden=1
autocmd StdinReadPre * let s:std_in=1

autocmd VimEnter * if argc() == 0 &&
      \  !exists("s:std_in") | NERDTree | endif

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 &&
      \  isdirectory(argv()[0]) &&
      \  !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
autocmd bufenter * if (winnr("$") == 1 &&
      \  exists("b:NERDTree") &&
      \  b:NERDTree.isTabTree()) | q | endif

autocmd! vimenter * NERDTree | wincmd w

"lightline
"disable regular insert
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'one',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

if !has('gui_running')
  set t_Co=256
endif

"fix slow scrolling
set lazyredraw

"horizontal line on cursor
"set cursorline!

"neoformat
let g:neoformat_basic_format_align = 1
let g:neoformat_basic_format_retab = 1
let g:neoformat_basic_format_trim = 1

"neoformat json
let g:neoformat_enabled_json = ['prettier']

augroup nvim_term
  au!
  au TermOpen * startinsert
  au TermClose * stopinsert
augroup END

"fugitive
set previewheight=30
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gw :Gwrite<CR>
nnoremap <leader>gr :Gread<CR>

"windows
nnoremap <leader>wd :q<CR>

"switch
nnoremap <leader>wh <C-W><C-H>
nnoremap <leader>wj <C-W><C-J>
nnoremap <leader>wk <C-W><C-k>
nnoremap <leader>wl <C-W><C-l>

"buffer
nnoremap <leader>fs :Neoformat<CR>:w<CR>
"nnoremap <leader>ff :Neoformat<CR>
nnoremap <leader>fS :wa<CR>
nnoremap <leader>fr :AckFile!<Space>
nnoremap <leader>bd :bd<CR>
nnoremap <leader>bn :bn<CR>
nnoremap <leader>bp :bp<CR>
nnoremap <leader>bb :buffer<Space>

"errors
nnoremap <leader>ej :ALENext<CR>
nnoremap <leader>ek :ALEPrevious<CR>
nnoremap <leader>ed :ALEDetail<CR>

"project
nnoremap <leader>pt :NERDTreeToggle<CR>
nnoremap <leader>pr :Ack!<Space>

"vim-commentary
autocmd FileType hs setlocal commentstring=--\ %s

"vim-indent-guides
nnoremap <leader>it :IndentGuidesToggle<CR>
let g:indent_guides_enable_on_vim_startup=0
hi IndentGuidesOdd  ctermbg=11
hi IndentGuidesEven ctermbg=15
let g:indent_guides_guide_size=1

"ack.vim
let g:ackprg = 'ag --vimgrep'
