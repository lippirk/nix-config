{ pkgs ? import <nixpkgs> {} }:
let

  homePrograms = with pkgs;
  [
    my-nvim
    my-tmux

    zip
    gnumake
    quassel
    ag
    tmuxinator
    editorconfig-core-c
    gitAndTools.gitFull
    vscode
    sqlitebrowser

    # haskell
    haskellPackages.hpack
    cabal2nix
    stack
    cabal-install
    z3
    haskellPackages.hlint
    haskellPackages.hfmt
    haskellPackages.ghcid
    haskellPackages.hoogle

  ] ++ pkgs.my-nvim-dependencies;

in homePrograms
