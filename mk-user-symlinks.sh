set -e

cp -asf ~/nix-config/overlays/ ~/.config/nixpkgs/
cp -asf ~/nix-config/user/config.nix ~/.config/nixpkgs/config.nix
cp -asf ~/nix-config/user/nix.conf ~/.config/nix/nix.conf
cp -asf ~/nix-config/user/xfce4/ ~/.config/
