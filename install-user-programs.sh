# install programs from list, removing anything that was previously
# installed using nix-env
nix-env -f ~/nix-config/user/default.nix -i --remove-all

# if you want to install from an attribute instead:
# nix-env -f ~/nix-config/user/default.nix -A my-attr -i --remove-all

# symlink gitconfig
ln -f ~/nix-config/user/gitconfig ~/.gitconfig
